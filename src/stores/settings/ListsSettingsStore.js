import StringsResourcesStore from "../resources/StringsResourcesStore"

class ListsSettingsStore {
    constructor() {
        this.strings = new StringsResourcesStore()
    }
    
    lists = {
        person: 'person',
        group: 'group',
        proficiency: 'proficiency',
        beforeChrist: 'beforeChrist',
        roughlyPeriod: 'roughlyPeriod',
        source: 'source',
        sourceMedium: 'sourceMedium',
        sourceType: 'sourceType',
        sourceTitle: 'sourceTitle',
        sourceDesc: 'sourceDesc',
        sourceUrl: 'sourceUrl',
        sourceDownloadDate: 'sourceDownloadDate',
        sourceAuthor: 'sourceAuthor',
        sourceWork: 'sourceWork',
        sourcePublisher: 'sourcePublisher',
        sourcePlace: 'sourcePlace',
        sourceInstitution: 'sourceInstitution',
        sourceLanguage: 'sourceLanguage',
        sourceRights: 'sourceRights',
        sourceExtra: 'sourceExtra',
    }

    categories = [
        'epoch',
        'event',
        'group',
        'person',
        'animal',
        'region',
        'village',
        'building',
        'item',
        'element',
        'book',
        'list',
        'subject',
    ]

    profile = 'profile'

    // routes
    routesLoadItem = [
        //'profile',
    ]

    routesLoadList = [
        'epoch',
        'event',
        'group',
        'person',
        'animal',
        'region',
        'village',
        'building',
        'item',
        'element',
        'book',
        'list',
        'subject',
        'users',
    ]

    routesWithIdLoadItem = [
        'epoch',
        'event',
        'group',
        'person',
        'animal',
        'region',
        'village',
        'building',
        'item',
        'element',
        'book',
        'list',
        'subject',
        //'profile',
    ]

    routesWithIdLoadList = [
        'users',
    ]
    
    types = {
        inpNoDropdown: 'inpNoDropdown',
        inpWithDropdown: 'inpWithDropdown',
        inpFromDropdown: 'inpFromDropdown',
        inpFromModalDropdown: 'inpFromModalDropdown',
        btnDefault: 'btnDefault',
        btnGroup: 'btnGroup',
        btnSource: 'btnSource',
        btnDateRoughly: 'btnDateRoughly',
        accDefault: 'accDefault',
    }

    source = {
        bible: 'bible',
        other: 'other',
    }

    sourceOther = {
        book: 'book',
        doc: 'doc',
        article: 'article',
        report: 'report',
        encyclopedia: 'encyclopedia',
        forum: 'forum',
        lecture: 'lecture',
        dictionary: 'dictionary',
        multimedia: 'multimedia',
        other: 'other',
    }

    beforeChrist = () => [
        { key: 'true', value: this.strings.t('beforeChrist'), text: this.strings.t('beforeChrist') },
        { key: 'false', value: this.strings.t('afterChrist'), text: this.strings.t('afterChrist') },
    ]

    roughlyPeriod = (value) => {
        if (this.roughlyPeriodSingular().includes(value))
            return [
                { key: 'day', value: this.strings.t('day'), text: this.strings.t('day') },
                { key: 'month', value: this.strings.t('month'), text: this.strings.t('month') },
                { key: 'year', value: this.strings.t('year'), text: this.strings.t('year') },
            ]
        else
            return [
                { key: 'day', value: this.strings.t('days'), text: this.strings.t('days') },
                { key: 'month', value: this.strings.t('months'), text: this.strings.t('months') },
                { key: 'year', value: this.strings.t('years'), text: this.strings.t('years') },
            ]
    }

    roughlyPeriodSingular = () => [
        this.strings.t('day'),
        this.strings.t('month'),
        this.strings.t('year'),
    ]
}

export default ListsSettingsStore