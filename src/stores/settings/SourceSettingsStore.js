import StringsResourcesStore from "../resources/StringsResourcesStore";
import ListsSettingsStore from "./ListsSettingsStore";

class SourceSettingsStore {
    constructor() {
        this.strings = new StringsResourcesStore()
        this.listsSettings = new ListsSettingsStore()
        this.types = this.listsSettings.types
        this.lists = this.listsSettings.lists
    }

    books_at = {
        "1. Mose": { //50
            1: 35,
            2: 3453,
        },
        "2. Mose": 40,
        "3. Mose": 27,
        "4. Mose": 36,
        "5. Mose": 34,
        "Josua": 24,
        "Richter": 21,
        "Ruth": 4,
        "1. Samuel": 31,
        "2. Samuel": 24,
        "1. Könige": 22,
        "2. Könige": 25,
        "Esra": 10,
        "Nehemia": 13,
        "Esther": 10,
        "Sprüche": 31,
        "Prediger": 12,
        "Daniel": 12,
        "Hosea": 14,
        "Joel": 4,
        "Amos": 9,
        "Obadja": 1,
        "Jona": 4,
        "Micha": 7,
        "Nahum": 3,
        "Habakuk": 3,
        "Zephanja": 3,
        "Haggai": 1,
        "Maleachi": 3
    }
    books_nt = {
        "Matthäus": 28,
        "Markus": 16,
        "Lukas": 24,
        "Johannes": 21,
        "Apostelgeschichte": 28,
        "Römer": 16,
        "1. Korinther": 16,
        "2. Korinther": 13,
        "Epheser": 6,
        "Philipper": 4,
        "Offenbarung": 22
    }
    

    get = () => [
        {
            id: 'source',
            label: 'Quelle',
            width: 10,
            info: 'Hier wählst du die Quellen-Art.',
            component: [
                {
                    type: this.types.inpFromDropdown,
                    list: this.lists.source,
                },
            ],
        },
        {
            id: 'medium',
            label: 'Medium',
            width: 6,
            info: 'Hier wählst du das Medium.',
            component: [
                {
                    type: this.types.inpFromDropdown,
                    list: this.lists.sourceMedium,
                },
            ],
        },
        //{
        //    id: 'type',
        //    label: 'Art',
        //    width: 6,
        //    info: 'Hier wählst du die Vortragsart.',
        //    component: [
        //        {
        //            type: this.types.inpFromDropdown,
        //            list: this.lists.sourceType,
        //        },
        //    ],
        //},
        {
            id: 'title',
            label: 'Titel',
            width: 8,
            info: 'Der Titel der Quelle.',
            errorText: 'Eine Quelle muss einen Titel haben.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceTitle,
                },
            ],
        },
        {
            id: 'desc',
            label: 'Beschreibung',
            width: 8,
            info: 'Du kannst der Quelle eine Beschreibung hinzufügen.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceDesc,
                },
            ],
        },
        {
            id: 'url',
            label: 'Url',
            width: 8,
            info: 'Hier kannst du die Quelle verlinken.',
            errorText: 'Die Url muss gültig sein.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    // eslint-disable-next-line
                    pattern: 'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)',
                    prefix: 'https://'
                },
            ],
        },
        {
            id: 'downloadDate',
            label: 'Abruf-Datum',
            width: 8,
            info: 'Wähle das Datum, an dem du deine Quelle abgerufen hast.',
            errorText: 'Muss ein Datum sein.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    pattern: '^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$',
                },
            ],
        },
        {
            id: 'author',
            label: 'Autor',
            width: 8,
            info: 'Schreibe denn Autor der Quelle.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceAuthor,
                },
            ],
        },
        {
            id: 'work',
            label: 'Name des Werks',
            width: 8,
            info: 'Hier kannst du das übergeordnete Werk hinschreiben.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceWork,
                },
            ],
        },
        {
            id: 'version',
            label: 'Ausgabe',
            width: 8,
            info: 'Schreibe die Nummer der Ausgabe.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    pattern: '[0-9]',
                },
            ],
        },
        {
            id: 'site',
            label: 'Seitennummer',
            width: 8,
            info: 'Schreibe die Seitennummer.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    pattern: '[0-9]',
                },
            ],
        },
        //{
        //    id: 'timespan',
        //    label: 'Zeitspanne',
        //    width: 8,
        //    info: 'Schreibe die Zeitspanne.',
        //    component: [
        //        {
        //            type: this.types.inpNoDropdown,
        //            pattern: '[0-9]-[0-9]',
        //        },
        //    ],
        //},
        {
            id: 'publisher',
            label: 'Verlag',
            width: 8,
            info: 'Halte den Verlag fest.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourcePublisher,
                },
            ],
        },
        {
            id: 'edition',
            label: 'Auflage',
            width: 8,
            info: 'Schreibe die Nummer der Auflage.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    pattern: '[0-9]',
                },
            ],
        },
        {
            id: 'place',
            label: 'Ort',
            width: 8,
            info: 'Schreibe den Ort deiner Quelle hin.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourcePlace,
                },
            ],
        },
        {
            id: 'institution',
            label: 'Institution',
            width: 8,
            info: 'Schreibe die Institution deiner Quelle hin.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceInstitution,
                },
            ],
        },
        {
            id: 'language',
            label: 'Sprache',
            width: 8,
            info: 'Wähle deine Sprache.',
            component: [
                {
                    type: this.types.inpFromDropdown,
                    list: this.lists.sourceLanguage,
                    noResult: 'Du musst eine Sprache aus der Liste wählen',
                },
            ],
        },
        {
            id: 'publishingDate',
            label: 'Erscheinungsdatum',
            width: 8,
            info: 'Wähle das Datum, an dem deine Quelle veröffentlicht wurde.',
            errorText: 'Muss ein Datum sein.',
            component: [
                {
                    type: this.types.inpNoDropdown,
                    pattern: '^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$',
                },
            ],
        },
        {
            id: 'isbn',
            label: 'Identifikationsnummer',
            width: 8,
            info: 'Mache deine Quelle eindeutig.',
            component: [
                {
                    id: 'isbn',
                    type: this.types.btnGroup,
                    component: [
                        {
                            id: 'isbn',
                            type: this.types.btnDefault,
                            name: 'ISBN',
                        },
                        {
                            id: 'doi',
                            type: this.types.btnDefault,
                            name: 'DOI',
                        },
                        {
                            id: 'issn',
                            type: this.types.btnDefault,
                            name: 'ISSN',
                        },
                    ],
                },
                {
                    id:'number',
                    type: this.types.inpNoDropdown,
                    pattern: '[0-9]',
                },
            ],
        },
        {
            id: 'rights',
            label: 'Rechte',
            width: 8,
            info: 'Hier kannst du Copyright oder dergleichen hinterlegen.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceRights,
                },
            ],
        },
        {
            id: 'extra',
            label: 'Extra',
            width: 16,
            info: 'Hier kannst du noch zusätzliche Infos ablegen.',
            component: [
                {
                    type: this.types.inpWithDropdown,
                    list: this.lists.sourceExtra,
                },
            ],
        },
    ]
}

export default SourceSettingsStore