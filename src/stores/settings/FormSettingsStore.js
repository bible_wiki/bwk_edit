import StringsResourcesStore from '../resources/StringsResourcesStore'
import ListsSettingsStore from './ListsSettingsStore'

class FormSettingsStore {
    constructor() {
        this.strings = new StringsResourcesStore()
        this.listsSettings = new ListsSettingsStore()
        this.types = this.listsSettings.types
        this.lists = this.listsSettings.lists
        this.beforeChrist = this.listsSettings.beforeChrist
    }
    

    field = (settings = {
        id: 'standart',
        label: this.strings.t('defaultLabel'),
        info: this.strings.t('defaultInfo'),
        }, component=null, additional=null) => {
            
        let generatedComponent = new Array()
        component !== null && component.map(val => {
            if (val.type === this.types.btnGroup) {
                generatedComponent.push(val)
            } else if (val.type.substring(0,3) === 'inp') {
                generatedComponent.push({
                    id: val.id,
                    type: val.type,
                    value: val.value, // optional
                    onlyKey: val.onlyKey, // optional
                    list: val.list || null, // optional --> only Dropdown
                    htmlType: val.htmlType || 'text', // optional
                    placeholder: val.placeholder || null, // optional
                    multiple: val.multiple || false, // optional
                    pattern: val.pattern || this.strings.t('defaultPattern'), // optional
                    style: val.style || null, // optional
                    noResult: val.noResult || null, // optional
                    onlyChoose: val.onlyChoose || false, // optional
                    disabled: settings.disabled || false, // optional
                })
            } else if (val.type.substring(0,3) === 'btn') {
                generatedComponent.push({
                    id: val.id,
                    type: val.type,
                    icon: val.icon || null, // optional
                    name: val.name || null, // optional
                })
            }
        })
        settings.showSource !== false && generatedComponent.push({
            id: settings.id + 'Source',
            type: this.types.btnSource,
        })
        return {
            id: settings.id,
            label: settings.label,
            width: settings.width || 8,
            info: settings.info,
            errorText: settings.errorText || null, // optional
            component: generatedComponent,
            additional: additional === null ? null : {
                id: additional.id,
                accLabel: additional.accLabel,
                fields: additional.fields
            }
        }
    }

    fieldNoDropdown = (settings, additional) => this.field(settings, [
        {
            id: settings.id,
            type: this.types.inpNoDropdown,
            value: settings.value,
            onlyKey: settings.onlyKey,
            htmlType: settings.htmlType,
            placeholder: settings.placeholder,
            pattern: settings.pattern,
            noResult: settings.noResult,
        },
    ], additional)

    fieldWithDropdown = (settings, additional) => this.field(settings, [
        {
            id: settings.id,
            type: this.types.inpWithDropdown,
            value: settings.value,
            onlyKey: settings.onlyKey,
            list: settings.list,
            htmlType: settings.htmlType,
            placeholder: settings.placeholder,
            multiple: settings.multiple,
            pattern: settings.pattern,
            noResult: settings.noResult,
        },
    ], additional)

    fieldFromDropdown = (settings, additional) => this.field(settings, [
        {
            id: settings.id,
            type: this.types.inpFromDropdown,
            value: settings.value,
            onlyKey: settings.onlyKey,
            list: settings.list,
            htmlType: settings.htmlType,
            placeholder: settings.placeholder,
            multiple: settings.multiple,
            pattern: settings.pattern,
            noResult: settings.noResult,
        },
    ], additional)

    fieldFromModalDropdown = (settings, additional) => this.field(settings, [
        {
            id: settings.id,
            type: this.types.inpFromModalDropdown,
            value: settings.value,
            onlyKey: settings.onlyKey,
            list: settings.list,
            htmlType: settings.htmlType,
            placeholder: settings.placeholder,
            multiple: settings.multiple,
            pattern: settings.pattern,
            noResult: settings.noResult,
        },
    ], additional)

    fieldOptions = (settings) => this.field(settings, [
        {
            id: settings.id,
            type: this.types.btnGroup,
            component: settings.options.map(val => {return {
                id: settings.id + val.id,
                type: settings.type,
                icon: val.icon,
                name: val.name,
            }})
        },
    ])

    fieldDate = (settings) => this.field(settings, [
        {
            id: settings.id + 'Day',
            type: this.types.inpNoDropdown,
            placeholder: this.strings.t('day'),
            pattern: '[0-9]{1,2}',
            style: {width: 300},
        },
        {
            id: settings.id + 'Month',
            type: this.types.inpNoDropdown,
            placeholder: this.strings.t('month'),
            pattern: '[0-9]{1,2}',
            style: {width: 300},
        },
        {
            id: settings.id + 'Year',
            type: this.types.inpNoDropdown,
            placeholder: this.strings.t('year'),
            pattern: '[0-9]{1,4}',
            style: {width: 400},
        },
        {
            id: settings.id + 'BeforeChrist',
            type: this.types.inpFromDropdown,
            list: this.lists.beforeChrist,
            onlyChoose: true,
        },
        {
            id: settings.id + 'Roughly',
            type: this.types.btnDateRoughly,
        },
    ])


    // categories
    person = () => [
        this.fieldNoDropdown({
            id: 'name',
            label: this.strings.t('nameLabel'),
            list: this.lists.person,
            info: this.strings.t('nameInfo'),
            disabled: true,
        },
        {
            id: 'additionalName',
            accLabel: this.strings.t('additionalNameLabel'),
            fields: [
                this.fieldWithDropdown({
                    id: 'visibleName',
                    label: this.strings.t('visibleNameLabel'),
                    list: this.lists.person,
                    info: this.strings.t('visibleNameInfo'),
                    placeholder: this.strings.t('visibleNamePlaceholder'),
                    multiple: true,
                }),
                this.fieldWithDropdown({
                    id: 'hiddenName',
                    label: this.strings.t('hiddenNameLabel'),
                    list: this.lists.person,
                    info: this.strings.t('hiddenNameInfo'),
                    placeholder: this.strings.t('hiddenNamePlaceholder'),
                    multiple: true,
                }),
            ]
        }),
        this.fieldNoDropdown({
            id: 'description',
            label: this.strings.t('descriptionLabel'),
            info: this.strings.t('descriptionInfo'),
            errorText: this.strings.t('descriptionErrortext'),
            placeholder: this.strings.t('descriptionPlaceholder'),
            pattern: this.strings.t('descriptionPattern'),
        }),
        this.fieldOptions({
            id: 'gender',
            label: this.strings.t('genderLabel'),
            type: this.types.btnDefault,
            options: [
                {
                    id: 'Male',
                    name: this.strings.t('genderMale'),
                },
                {
                    id: 'Female',
                    name: this.strings.t('genderFemale'),
                },
                {
                    id: 'Unknow',
                    name: this.strings.t('genderUnknow'),
                },
            ]
        }),
        this.fieldOptions({
            id: 'believer',
            label: this.strings.t('believerLabel'),
            type: this.types.btnDefault,
            options: [
                {
                    id: 'Is',
                    name: this.strings.t('believerIs'),
                },
                {
                    id: 'No',
                    name: this.strings.t('believerNo'),
                },
                {
                    id: 'Unknow',
                    name: this.strings.t('believerUnknow'),
                },
            ]
        }),
        this.fieldFromModalDropdown({
            id: 'proficiency',
            label: this.strings.t('proficiencyLabel'),
            list: this.lists.proficiency,
            info: this.strings.t('proficiencyInfo'),
            onlyKey: true,
        }),
        this.fieldFromModalDropdown({
            id: 'group',
            label: this.strings.t('groupLabel'),
            list: this.lists.group,
            info: this.strings.t('groupInfo'),
            onlyKey: true,
        }),
        this.fieldDate({
            id: 'birth',
            label: this.strings.t('birthLabel'),
            listChrist: this.lists.christ,
            listPeriod: this.lists.period,
            info: this.strings.t('birthInfo'),
        }),
    ]

    groupNew = () => [
        this.fieldFromModalDropdown({
            id: 'name',
            label: this.strings.t('nameLabel'),
            list: this.lists.person,
            info: this.strings.t('nameInfo'),
            value: '',
        }),
        this.fieldNoDropdown({
            id: 'description',
            label: this.strings.t('descriptionLabel'),
            info: this.strings.t('descriptionInfo'),
            errorText: this.strings.t('descriptionErrortext'),
            placeholder: this.strings.t('descriptionPlaceholder'),
            pattern: this.strings.t('descriptionPattern'),
            value: '',
        }),
    ]

    personNew = () => [
        this.fieldFromModalDropdown({
            id: 'name',
            label: this.strings.t('nameLabel'),
            list: this.lists.person,
            info: this.strings.t('nameInfo'),
            value: '',
        }),
    ]
}

/*person = [
    {
        id: 'name',
        label: 'Name',
        width: 8,
        info: 'Schreibweise von Schlachter 2000.',
        component: [
            {
                id: 'name',
                type: this.types.inpWithDropdown,
                list: this.lists.person,
            },
            {
                id: 'nameSource',
                type: this.types.btnSource,
            },
        ],
        additional: {
            id: 'additionalName',
            accLabel: 'Weitere Namen',
            fields: [
                {
                    id: 'visibleName',
                    label: 'Zusätzliche Namen (sichtbar)',
                    info: 'Diese zusätzliche Namen sind sichtbar.',
                    component: [
                        {
                            id: 'visibleName',
                            type: this.types.inpWithDropdown,
                            list: this.lists.person,
                            placeholder: 'Sichtbar',
                            multiple: true,
                        },
                        {
                            id: 'visibleNameSource',
                            type: this.types.btnSource,
                        },
                    ],
                },
                {
                    id: 'hiddenName',
                    label: 'Andere Schreibweisen (unsichtbar)',
                    info: 'Diese Namen sind nirgens ersichtlich, sie werden nur für die Suche verwendet.',
                    component: [
                        {
                            id: 'hiddenName',
                            type: this.types.inpWithDropdown,
                            list: this.lists.person,
                            placeholder: 'Unsichtbar',
                            multiple: true,
                        },
                        {
                            id: 'hiddenNameSource',
                            type: this.types.btnSource,
                        },
                    ],
                },
            ],
        },
    },
    {
        id: 'description',
        label: 'Eindeutigkeit',
        width: 8,
        info: 'Dieses Feld dient zur Eindeutigkeit der Person.',
        errorText: 'Sollte weniger als 20 Zeichen enthalten.',
        component: [
            {
                id: 'description',
                type: this.types.inpNoDropdown,
                placeholder: 'Max 20 Zeichen',
                pattern: '/[A-Za-z0-9-. ÄÖÜäöü]{1,20}/g',
            },
            {
                id: 'descriptionSource',
                type: this.types.btnSource,
            },
        ],
    },
    {
        id: 'gender',
        label: 'Geschlecht',
        width: 8,
        component: [
            {
                id: 'gender',
                type: this.types.btnGroup,
                component: [
                    {
                        id: 'genderMale',
                        type: this.types.btnDefault,
                        name: 'Mann',
                    },
                    {
                        id: 'genderFemale',
                        type: this.types.btnDefault,
                        name: 'Frau',
                    },
                    {
                        id: 'unknow',
                        type: this.types.btnDefault,
                        name: 'Unklar',
                    },
                ],
            },
            {
                id: 'genderSource',
                type: this.types.btnSource,
            },
        ],
    },
    {
        id: 'believer',
        label: 'Gottesfurcht',
        width: 8,
        component: [
            {
                id: 'believer',
                type: this.types.btnGroup,
                component: [
                    {
                        id: 'believer',
                        type: this.types.btnDefault,
                        name: 'Gläubig',
                    },
                    {
                        id: 'notBeliever',
                        type: this.types.btnDefault,
                        name: 'Ungläubig',
                    },
                    {
                        id: 'unknow',
                        type: this.types.btnDefault,
                        name: 'Unklar',
                    },
                ],
            },
            {
                id: 'believerSource',
                type: this.types.btnSource,
            },
        ],
    },
    {
        id: 'description',
        label: 'Eindeutigkeit',
        width: 8,
        info: 'Dieses Feld dient zur Eindeutigkeit der Person.',
        errorText: 'Sollte weniger als 20 Zeichen enthalten.',
        component: [
            {
                id: 'description',
                type: this.types.inpFromModalDropdown,
                placeholder: 'Max 20 Zeichen',
                pattern: '/[A-Za-z0-9-. ÄÖÜäöü]{1,20}/g',
            },
            {
                id: 'descriptionSource',
                type: this.types.btnSource,
            },
        ],
    },
]*/

export default FormSettingsStore