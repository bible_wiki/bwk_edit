import { observable, action, autorun } from 'mobx'
import EntryStore from './data/EntryStore'
import StringsResourcesStore from './resources/StringsResourcesStore'

class BaseStore {
    constructor() {
        this.entry = new EntryStore(this)
        this.strings = new StringsResourcesStore()
    }

    // ---------------------------------- STATES ----------------------------------
    // loading
    @observable
    loading = true
    loaded = false

    @action
    startLoad = () => {
        if (this.loading && !this.loaded) {
            this.loading = true
            setTimeout(() => this.endLoad(), 1000)
        }
    }

    @action
    endLoad = () => {
        this.loading = false
        this.loaded = true
    }

    // INFOS
    @observable
    path = null

    @observable
    category = null

    @observable
    id = null

    @observable
    name = null
    
    @observable
    desc = null
    
    @observable
    title = null

    changeCategoryAuto = autorun(() => {
        this.path !== null &&
            (this.category = this.strings.t('titles')[this.path])
    })

    changeNameAuto = autorun(() => {
        this.id === null ?
            this.path !== null &&
                (this.name = this.strings.t('titles')[this.path]) :
            (this.name = Object.keys(this.entry.person.name)[0])
    })
    
    changeDescAuto = autorun(() => {
        this.id !== null &&
            (this.desc = Object.keys(this.entry.person.description)[0])
    })
    
    changeTitleAuto = autorun(() => {
        this.title = this.name
        this.desc !== null &&
            (this.title += ' ' + this.desc)
    })
    

    // ---------------------------------- SETTINGS ----------------------------------
    
    // mode
    TABLE = 'table'
    CARDS = 'cards'

    @observable
    mode = this.CARDS

    @action
    toggleMode = () => this.mode = this.mode === this.TABLE ? this.CARDS : this.TABLE
    
    // description permission
    @observable
    inpDescEnable = true

    @action
    toggleInpDesc = () => this.inpDescEnable = this.inpDescEnable === true ? false : true

    @action
    deactivateInpDesc = () => this.inpDescEnable = false

    @action
    activateInpDesc = () => this.inpDescEnable = true


    // <10 --> Entwurf
    // 11-20 --> Freigegeben
    // 21-30 --> Lektor
    // 31< --> Veröffentlicht
    @observable
    state = 1

    @action
    saveEntry = () => console.log('save entry')

    @action
    stateDraft = () => {
        this.state = 1
        this.saveEntry()
    }

    @action
    stateOpen = () => {
        this.state = 11
        this.saveEntry()
    }

    @action
    stateCommit = () => {
        this.state = 21
        this.saveEntry()
    }

    @action
    statePublish = () => {
        this.state = 31
        this.saveEntry()
    }
}

export default BaseStore