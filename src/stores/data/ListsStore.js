import { observable } from "mobx"

class ListsStore {

    // categories
    @observable
    person = [
        { key: '1', value: 'Petrus', text: 'Petrus' },
        { key: '2', value: 'Daniel', text: 'Daniel' },
        { key: '3', value: 'David', text: 'David' },
    ]
    
    @observable
    group = [
        { key: '1', value: 'Juda', text: 'Juda' },
        { key: '2', value: 'Israel', text: 'Israel' },
        { key: '3', value: 'oder so', text: 'oder so' },
    ]

    // additional
    @observable
    proficiency = [
        { key: '1', value: 'Hirte', text: 'Hirte' },
        { key: '2', value: 'Bauer', text: 'Bauer' },
        { key: '3', value: 'König', text: 'König' },
    ]

    // source
    @observable
    source = [
        { key: '1', value: 'Dokument', text: 'Dokument' },
        { key: '2', value: 'Vortrag', text: 'Vortrag' },
    ]

    @observable
    sourceMedium = [
        { key: '1', value: 'Film', text: 'Film' },
        { key: '2', value: 'Podcast', text: 'Podcast' },
    ]

    @observable
    sourceType = [
        { key: '1', value: 'Audio', text: 'Audio' },
        { key: '2', value: 'Filmvortrag', text: 'Filmvortrag' },
    ]

    @observable
    sourceTitle = [
        { key: '1', value: 'Es war ein reiches Leben', text: 'Es war ein reiches Leben' },
        { key: '2', value: 'Seiner Spur folgen', text: 'Seiner Spur folgen' },
    ]

    @observable
    sourceDesc = [
        { key: '1', value: 'Revidierte Version', text: 'Revidierte Version' },
    ]

    @observable
    sourceAuthor = [
        { key: '1', value: 'Spurgeon', text: 'Spurgeon' },
    ]

    @observable
    sourceWork = [
        { key: '1', value: 'Enzyklopedia Europa', text: 'Enzyklopedia Europa' },
    ]

    @observable
    sourcePublisher = [
        { key: '1', value: 'CSV', text: 'CSV' },
    ]

    @observable
    sourcePlace = [
        { key: '1', value: 'Bern', text: 'Bern' },
    ]

    @observable
    sourceInstitution = [
        { key: '1', value: 'Wycliff', text: 'Wycliff' },
    ]

    @observable
    sourceLanguage = [
        { key: '1', value: 'Deutsch', text: 'Deutsch' },
        { key: '2', value: 'Englisch', text: 'Englisch' },
    ]

    @observable
    sourceRights = [
        { key: '1', value: 'Kopiergeschützt', text: 'Kopiergeschützt' },
    ]

    @observable
    sourceExtra = [
        { key: '1', value: 'Ist eine tolles Buch.', text: 'Ist eine tolles Buch.' },
    ]
}

export default ListsStore