import { observable, action } from "mobx"
import StringsResourcesStore from "../resources/StringsResourcesStore"

class EntryStore {
    constructor(base) {
        this.base = base
        this.strings = new StringsResourcesStore()
    }

    @action
    getValue = (fieldId, inputId = false, coded = false) => {
        try {
            if (inputId) {
                const value = Object.keys(this[this.base.path][fieldId])
                if (inputId.includes('Day')) return this.getDay(value)
                if (inputId.includes('Month')) return this.getMonth(value)
                if (inputId.includes('Year')) return this.getYear(value)
                if (inputId.includes('BeforeChrist')) return this.getBeforeChrist(value)
                if (inputId.includes('Roughly')) return this.calcShowingRoughly(value, coded)
                return value
            } else return Object.keys(this[this.base.path][fieldId])
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nThe field with the id ' + fieldId + ' has not a value')
        }
    }

    getDay = (value) => value[0].split(':')[0].split('-')[2]
    getMonth = (value) => value[0].split(':')[0].split('-')[1]
    getYear = (value) => value[0].split(':')[0].split('-')[0]
    getBeforeChrist = (value) => {
        if (value[0].split(':')[1] === 'true') return this.strings.t('beforeChrist')
        return this.strings.t('afterChrist')
    }

    calcShowingRoughly = (value, coded) => { //TODO give zero correct
        Array.isArray(value) && (value = value[0])
        let roughly = typeof value === 'string' ? value.split(':')[2] || '0' : '0'
        roughly = Number(roughly) || 0
    
        if (roughly === 0) return coded ? { numb: '', period: 'y' } : roughly
        if (roughly < 30) return this.isDay(roughly, coded)
        if (roughly < 360) return this.isMonth(roughly, coded)
        return this.isYear(roughly, coded)
    }
    isDay = (numb, coded) => coded ? { numb: numb, period: 'day' } : numb + this.strings.t('dayShort')
    isMonth = (numb, coded) => {
        if (numb % 30 !== 0) return this.isDay(numb, coded)
        return coded ? { numb: (numb / 30), period: 'month' } : (numb / 30) + this.strings.t('monthShort')
    }
    isYear = (numb, coded) => {
        if (numb % 360 !== 0) return this.isMonth(numb, coded)
        return coded ? { numb: (numb / 360), period: 'year' } : (numb / 360) + this.strings.t('yearShort')
    }

    @action
    setValue = (fieldId, value, inputId = false) => {
        try {
            const field = this[this.base.path][fieldId]
            if (Array.isArray(value)) {
                const oldKeys = Object.keys(field)
                // add new value
                for (let i=0; i<value.length; i++) {
                    if (!oldKeys.includes(value[i])) {
                        field[value[i]] = {}
                    }
                }
                // remove the old value
                for (let i=0; i<oldKeys.length; i++) {
                    if (!value.includes(oldKeys[i])) {
                        let deleted = oldKeys.filter(x => !value.includes(x));
                        delete field[deleted]
                    }
                }
            } else {
                // replace value
                const oldValue = Object.keys(field)[0]
                if (value !== undefined) {
                    if (inputId) {
                        //TODO set correct
                        if (inputId.includes('Day')) value = this.setDay(value, oldValue)
                        if (inputId.includes('Month')) value = this.setMonth(value, oldValue)
                        if (inputId.includes('Year')) value = this.setYear(value, oldValue)
                        if (inputId.includes('BeforeChrist')) value = this.setBeforeChrist(value, oldValue)
                        if (inputId.includes('RoughlyNumb')) value = this.calcSaveRoughly(fieldId, value, oldValue, true)
                        if (inputId.includes('RoughlyPeriod')) value = this.calcSaveRoughly(fieldId, value, oldValue, false)
                    }
                    field[value] = field[oldValue]
                } else
                    field.empty = field[oldValue]
                delete field[oldValue]
            }
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\Can not set value to field with the id ' + fieldId)
        }
    }

    setDay = (value, oldValue) => {
        const date = oldValue.split(':')[0]
        const year = date.split('-')[0]
        const month = date.split('-')[1]
        const beforeChrist = oldValue.split(':')[1]
        const roughly = oldValue.split(':')[2]

        return year + '-' + month + '-' + (value === '' ? 0 : value) + ':' + beforeChrist + ':' + roughly
    }
    setMonth = (value, oldValue) => {
        const date = oldValue.split(':')[0]
        const year = date.split('-')[0]
        const day = date.split('-')[2]
        const beforeChrist = oldValue.split(':')[1]
        const roughly = oldValue.split(':')[2]

        return year + '-' + (value === '' ? 0 : value) + '-' + day + ':' + beforeChrist + ':' + roughly
    }
    setYear = (value, oldValue) => {
        const date = oldValue.split(':')[0]
        const month = date.split('-')[1]
        const day = date.split('-')[2]
        const beforeChrist = oldValue.split(':')[1]
        const roughly = oldValue.split(':')[2]

        return (value === '' ? 0 : value) + '-' + month + '-' + day + ':' + beforeChrist + ':' + roughly
    }
    setBeforeChrist = (value, oldValue) => {
        const date = oldValue.split(':')[0]
        const roughly = oldValue.split(':')[2]

        return date + ':' + (value === this.strings.t('beforeChrist') ? 'true' : 'false') + ':' + roughly
    }
    calcSaveRoughly = (fieldId, value, oldValue, isNumb) => {
        const date = oldValue.split(':')[0]
        const beforeChrist = oldValue.split(':')[1]
        let numb = ''
        let period = ''

        if (isNumb) {
            numb = value
            period = document.getElementsByName(fieldId + 'RoughlyPeriod')[0].children[0].innerText
        } else {
            numb = document.getElementsByName(fieldId + 'RoughlyNumb')[0].value
            period = value
        }

        if (period === this.strings.t('month') || period === this.strings.t('months'))
            numb = Number(numb) * 30
        if (period === this.strings.t('year') || period === this.strings.t('years'))
            numb = Number(numb) * 360

        return date + ':' + beforeChrist + ':' + numb
    }

    @action
    getSource = (fieldId) => {
        try {
            return this[this.base.path][fieldId] || {}
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nThe field with the id ' + fieldId + ' has not a source')
        }
    }

    @action
    setBibleSource = (fieldId, value, source) => {
        try {
            //TODO dont set to array...
            this[this.base.path][fieldId][value].bible.push(source)
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nCan not set bible-source to field with the id ' + fieldId)
        }
    }

    @action
    removeBibleSource = (fieldId, value, source) => {
        try {
            //TODO dont remove from array...
            this[this.base.path][fieldId][value].bible.remove(source)
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nCan not remove bible-source from field with the id ' + fieldId)
        }
    }

    @action
    setOtherSource = (fieldId, value, source) => {
        try {
            //TODO dont set to array???
            this[this.base.path][fieldId][value].other.push(source)
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nCan not set other-source to field with the id ' + fieldId)
        }
    }

    @action
    removeOtherSource = (fieldId, value, source) => {
        try {
            //TODO dont remove from array...
            let other = this[this.base.path][fieldId][value].other
            for(let i=0; i<other.length; i++){
                if (other[i].title === source) {
                    other.remove(other[i])
                    break
                }
            }
        } catch(e) {
            // eslint-disable-next-line
            console.log('>> BibleWiki <<\nCan not remove other-source from field with the id ' + fieldId)
        }
    }
    
    @observable
    person = {
        name: {
            'Petrus': {
                bible: [
                    '1.Mo 5,10',
                    '1.Mo 5,11',
                ],
                other: [
                    {
                        source: 'book',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                ],
            },
        },
        visibleName: {
            'Simon': {
                bible: [
                    '1.Mo 5,10',
                    '1.Mo 5,11',
                ],
                other: [
                    {
                        source: 'doc',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Spurgeon',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                ],
            },
            'Petros': {
                bible: [
                    '1.Mo 5,10',
                ],
                other: [
                    {
                        source: 'article',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel1',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                    {
                        source: 'report',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel2',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                    {
                        source: 'encyclopedia',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel3',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                ],
            },
        },
        hiddenName: {
            'Petruss': {
                bible: [
                    '1.Mo 5,10',
                    '1.Mo 5,11',
                ],
            },
            'Daniel': {},
            'David': {},
        },
        description: {
            'Apostel Christi': {
                other: [
                    {
                        source: 'lecture',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                ],
            },
        },
        gender: {
            1: {
                bible: [
                    '1.Mo 5,10',
                ],
            }
        },
        believer: {
            0: {
                bible: [
                    '1.Mo 5,10',
                ],
                other: [
                    {
                        source: 'dictionary',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                    {
                        source: 'multimedia',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                    {
                        source: 'other',
                        medium: 'Medium',
                        type: 'Art',
                        title: 'Titel',
                        desc: 'Beschreibung',
                        url: 'URL',
                        downloadDate: '10.05.2020',
                        author: 'Autor',
                        work: 'Name des Werks',
                        version: 'Ausgabe',
                        site: '153',
                        timespan: '12:55-13:65',
                        publisher: 'Verlag',
                        edition: 'Auflage',
                        place: 'Ort',
                        institution: 'Institution',
                        language: 'Deutsch',
                        publishingDate: '9.05.2020',
                        isbn: {
                            isbn: 0,
                            number: '654654654',
                        },
                        rights: 'Rechte',
                        extra: 'Extra',
                    },
                ],
            },
        },
        proficiency: {
            2: {},
        },
        group: {
            1: {},
        },
        birth: {
            '2020-5-8:true:45': {},
        },
    }
}

export default EntryStore