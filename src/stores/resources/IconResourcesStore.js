class IconResourcesStore {

    // state
    draft = 'privacy'
    open = 'inbox'
    commit = 'eraser'
    publish = 'paper plane'

    save = 'save'
    info = 'info'

    // source
    source = 'quote right'
    add = 'plus'
    remove = 'remove'
    arrowRight = 'arrow right'
    exist = 'copy'
    sourceOther = {
        book: 'bookmark',
        doc: 'file alternate',
        article: 'newspaper',
        report: 'bullhorn',
        encyclopedia: 'book',
        forum: 'comment',
        lecture: 'graduation cap',
        dictionary: 'book',
        multimedia: 'film',
        other: 'briefcase',
    }
}

export default IconResourcesStore