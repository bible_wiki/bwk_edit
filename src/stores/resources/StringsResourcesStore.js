import Doc from "../../gui/func/Doc"

class StringsResourcesStore {
    constructor() {
        this.availableLang = ['de', 'en']

        this.userLang = navigator.language || navigator.userLanguage

        if (this.availableLang.includes(this.userLang)) {
            Doc.lang(this.userLang)
            this.lang = this.userLang
        } else {
            this.lang = 'de'
        }
        for (let i=0; i<this.availableLang.length; i++) {
            this[this.availableLang[i]] = this.json(this.availableLang[i])
        }
    }

    t = (word) => this[this.lang][word] || this.de[word] || this.en[word]

    json = (lang) => require('./' + lang + '.json')
}

export default StringsResourcesStore