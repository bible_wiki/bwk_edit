import React from 'react'
import BaseStore from '../stores/BaseStore'
import StringsResourcesStore from '../stores/resources/StringsResourcesStore'
import IconResourcesStore from '../stores/resources/IconResourcesStore'
import ListsSettingsStore from '../stores/settings/ListsSettingsStore'
import FormSettingsStore from '../stores/settings/FormSettingsStore'
import SourceSettingsStore from '../stores/settings/SourceSettingsStore'
import ListsStore from '../stores/data/ListsStore'

export const storesContext = React.createContext({
	strings: new StringsResourcesStore(),
	icon: new IconResourcesStore(),
	base: new BaseStore(),
	formSettings: new FormSettingsStore(),
	sourceSettings: new SourceSettingsStore(),
	listsSettings: new ListsSettingsStore(),
	lists: new ListsStore(),
})