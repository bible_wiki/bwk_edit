import React from 'react'
import './App.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'
import { useStores } from './hooks/useStores'
import Overview from './gui/pages/Overview'
import Path from './gui/func/Path'
import PathWithId from './gui/func/PathWithId'
import BwkNavbar from './gui/components/BwkNavbar'

const App = () => {
	const { base } = useStores()
	base.startLoad()
	return (
		<Router>
			<Grid columns={2}>
				<Grid.Column className='navGrid'>
					<BwkNavbar />
				</Grid.Column>
				<Grid.Column className='mainGrid' >
					<Switch>
						<Route exact path="/" render={(props) => {
							base.path = props.match.params.path
							return <Overview />
						}} />
						<Route exact path="/:path" render={(props) => {
							base.path = props.match.params.path
							return <Path />
						}} />
						<Route path="/:path/:id" render={(props) => {
							base.path = props.match.params.path
							base.id = props.match.params.id
							return <PathWithId />
						}} />
					</Switch>
				</Grid.Column>
			</Grid>
		</Router>
	)
}

export default App
