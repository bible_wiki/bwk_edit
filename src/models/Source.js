class Source {
    constructor(data) {
        //this.bibleSources = new Array()
        List<Bible> bible
        this.data = data3
    }

    setBiblesource();
    
    getBiblesource();

    addBibleSource();
    remove();
}

class Bible {
    book = {
        100: 'Mt',
    }
    bookNumber
    chapter
    verse

    constructor(data) {
        Object.assign(this, data)
    }

    getBookNumber = () => this.bookNumber
    getBook = () => this.book[this.bookNumber]
    getChapter = () => this.chapter
    getVerse = () => this.verse

    setStelle = (bookNumber, chapter, verse) => {
        this.bookNumber = bookNumber
        this.chapter = chapter
        this.verse = verse
    }
}

export default Source