import Source from './Source'

class Person {
    constructor() {

    }

    name = new SimpleField(data.name)
    visibleName = MultipleField(data.visibleName)
    source = new Source(data)
}

class SimpleField {
    value
    source

    constructor(data) {
        this.value = data.value
        this.source = new Source(data.source)
    }
}

class MultipleField {
    simpleField = new Array()

    constructor(data) {
        data.forEach(one => {
            this.simpleField.push(one)
        });
    }
}