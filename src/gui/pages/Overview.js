import React from 'react'
import Doc from '../func/Doc'
import { useStores } from '../../hooks/useStores'

const Overview = () => {
    const { strings } = useStores()

    Doc.setDefault()

    return (
        <main>
            <h1>{strings.t('overview')}</h1>
        </main>
    )
}

export default Overview