import React from 'react'
import Doc from '../func/Doc'
import { Header, Button, Table, Card, Label, Container, Image, Icon } from 'semantic-ui-react'
import { useStores } from '../../hooks/useStores'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import img from '../../profile.png'
import { Fragment } from 'react'

const ListView = observer(() => {
	const { base, strings } = useStores()

	Doc.info(base.title, base.path, Doc.isCurrent)

	return (
		<main>
			<Header as='h2' style={{ padding: 30 }}>
				<Header.Content>
					{base.title}
					<Button.Group style={{ paddingLeft: 30 }}>
						<Button primary
							as={Link}
                    		to='/person/1234'>
							{strings.t('newEntry')}
            			</Button>
						<Button>
							{strings.t('myDrafts')}
            			</Button>
					</Button.Group>
				</Header.Content>
			</Header>
			<Container as={Link} to ='/person/1234'>
				{
					base.mode === base.TABLE ?
						<Table>
							<Table.Header>
								<Table.HeaderCell>Einträge</Table.HeaderCell>
								<Table.HeaderCell>Status</Table.HeaderCell>
								<Table.HeaderCell>Autor</Table.HeaderCell>
							</Table.Header>
							<Table.Body>
								<Table.Row>
									<Table.Cell>
										<Label ribbon>{strings.t('myEntry')}</Label>
										Petrus
									</Table.Cell>
									<Table.Cell>Entwurf</Table.Cell>
									<Table.Cell>Josua</Table.Cell>
								</Table.Row>
								<Table.Row>
									<Table.Cell>Andere Einträge</Table.Cell>
									<Table.Cell>Entwurf</Table.Cell>
									<Table.Cell>Johnny</Table.Cell>
								</Table.Row>
								<Table.Row>
									<Table.Cell>3. Eintrag</Table.Cell>
									<Table.Cell>Entwurf</Table.Cell>
									<Table.Cell>Johnny</Table.Cell>
								</Table.Row>
								<Table.Row>
									<Table.Cell>4. Eintrag</Table.Cell>
									<Table.Cell>Entwurf</Table.Cell>
									<Table.Cell>Johnny</Table.Cell>
								</Table.Row>
								<Table.Row>
									<Table.Cell>5. Eintrag</Table.Cell>
									<Table.Cell>Entwurf</Table.Cell>
									<Table.Cell>Johnny</Table.Cell>
								</Table.Row>
							</Table.Body>
						</Table> :
						<Card.Group>
							<Card>
								<Image src={img} wrapped ui={false} />
								<Card.Content>
									<Card.Header>Petrus</Card.Header>
									<Card.Meta>
										<span className='date'>Apostel Christi</span>
									</Card.Meta>
									<Card.Description>
										Eine Beschreibung oder so.
									</Card.Description>
								</Card.Content>
								<Card.Content extra>
									<Card.Description>
										<Icon name='user' /> Josua
									</Card.Description>
								</Card.Content>
							</Card>
							<Card>
								<Image src={img} wrapped ui={false} />
								<Card.Content>
									<Card.Header>Andere Einträge</Card.Header>
									<Card.Meta>
										<span className='date'>Eindeutigkeit</span>
									</Card.Meta>
									<Card.Description>
										Eine Beschreibung oder so.
									</Card.Description>
								</Card.Content>
								<Card.Content extra>
									<Card.Description>
										<Icon name='user' /> Johnny
									</Card.Description>
								</Card.Content>
							</Card>
							<Card>
								<Image src={img} wrapped ui={false} />
								<Card.Content>
									<Card.Header>3. Eintrag</Card.Header>
									<Card.Meta>
										<span className='date'>Eindeutigkeit</span>
									</Card.Meta>
									<Card.Description>
										Eine Beschreibung oder so.
									</Card.Description>
								</Card.Content>
								<Card.Content extra>
									<Card.Description>
										<Icon name='user' /> Johnny
									</Card.Description>
								</Card.Content>
							</Card>
							<Card>
								<Image src={img} wrapped ui={false} />
								<Card.Content>
									<Card.Header>4. Eintrag</Card.Header>
									<Card.Meta>
										<span className='date'>Eindeutigkeit</span>
									</Card.Meta>
									<Card.Description>
										Eine Beschreibung oder so.
									</Card.Description>
								</Card.Content>
								<Card.Content extra>
									<Card.Description>
										<Icon name='user' /> Johnny
									</Card.Description>
								</Card.Content>
							</Card>
							<Card>
								<Image src={img} wrapped ui={false} />
								<Card.Content>
									<Card.Header>5. Eintrag</Card.Header>
									<Card.Meta>
										<span className='date'>Eindeutigkeit</span>
									</Card.Meta>
									<Card.Description>
										Eine Beschreibung oder so.
									</Card.Description>
								</Card.Content>
								<Card.Content extra>
									<Card.Description>
										<Icon name='user' /> Johnny
									</Card.Description>
								</Card.Content>
							</Card>
						</Card.Group>
				}
			</Container>
		</main>
	)
});

export default ListView