import React from 'react'
import Doc from '../func/Doc'
import { useStores } from '../../hooks/useStores'

const NotFound = () => {
    const { strings } = useStores()

    Doc.info(strings.t('notfound'), Doc.isCurrent, '../../logo_notfound.svg')

    return (
        <main>
            <h1>{strings.t('notfound')}</h1>
        </main>
    )
}

export default NotFound