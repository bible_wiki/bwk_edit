import React, { Fragment } from 'react'
import Doc from '../func/Doc'
import { Grid, Form, Button, Dropdown, Icon, Step, Segment } from 'semantic-ui-react'
import BwkFormField from '../components/BwkFormField'
import { useStores } from '../../hooks/useStores'
import { observer } from 'mobx-react'

const setPageInfo = (base) => {
    if (base.path === 'profile') {
        Doc.info(Doc.isCurrent, base.path, Doc.isDefault)
    } else {
        Doc.info(base.title, base.path + '/' + base.id + '/' + base.title, Doc.isDefault)
    }
}

const SaveButtons = ({ base }) => {
    const { strings, icon } = useStores()
    return <Segment floated='right' basic>
        <Button
            onClick={base.saveEntry}
            style={{marginRight: 10}}
            disabled={base.loading}
            animated >
            <Button.Content visible>{strings.t('save')}</Button.Content>
            <Button.Content hidden><Icon name={icon.save} /></Button.Content>
        </Button>
        <Button.Group primary>
            {base.state <= 10 ?
                <Fragment>
                    <Button onClick={base.stateOpen} disabled={base.loading} animated>
                        <Button.Content visible>{strings.t('open')}</Button.Content>
                        <Button.Content hidden><Icon name={icon.open} /></Button.Content>
                    </Button>
                    <Dropdown
                        className='button icon'
                        floating
                        options={[
                            { key: 'commit', value: 'commit', icon: icon.commit, text: strings.t('commit'), onClick: base.stateCommit },
                            { key: 'publish', value: 'publish', icon: icon.publish, text: strings.t('publish'), onClick: base.statePublish },
                        ]}
                        disabled={base.loading}
                        trigger={<Fragment />}
                    />
                </Fragment> :
                base.state <= 20 ?
                    <Fragment>
                        <Button onClick={base.stateCommit} disabled={base.loading} animated>
                            <Button.Content visible>{strings.t('commit')}</Button.Content>
                            <Button.Content hidden><Icon name={icon.commit} /></Button.Content>
                        </Button>
                        <Dropdown
                            className='button icon'
                            floating
                            options={[
                                { key: 'publish', value: 'publish', icon: icon.publish, text: strings.t('publish'), onClick: base.statePublish },
                            ]}
                            disabled={base.loading}
                            trigger={<Fragment />}
                        />
                    </Fragment> :
                    base.state <= 30 ?
                        <Fragment>
                            <Button onClick={base.statePublish} disabled={base.loading} animated>
                                <Button.Content visible>{strings.t('publish')}</Button.Content>
                                <Button.Content hidden><Icon name={icon.publish} /></Button.Content>
                            </Button>
                            <Dropdown
                                className='button icon'
                                floating
                                options={[
                                    { key: 'open', value: 'open', icon: icon.open, text: strings.t('openAgain'), onClick: base.stateOpen },
                                ]}
                                disabled={base.loading}
                                trigger={<Fragment />}
                            />
                        </Fragment> :
                        <Fragment>
                            <Button onClick={base.statePublish} disabled={base.loading} animated>
                                <Button.Content visible>{strings.t('publishChanges')}</Button.Content>
                                <Button.Content hidden><Icon name={icon.publish} /></Button.Content>
                            </Button>
                            <Dropdown
                                className='button icon'
                                floating
                                options={[
                                    { key: 'commit', value: 'commit', icon: icon.commit, text: strings.t('unpublish'), onClick: base.stateCommit },
                                ]}
                                disabled={base.loading}
                                trigger={<Fragment />}
                            />
                        </Fragment>
            }
        </Button.Group>
    </Segment>
}

const OnTheWay = ({ state }) => {
    const { strings: lang, icon } = useStores()
    return (
        <Step.Group stackable='tablet' fluid>
            <Step
                active={state <= 10 ? true : false}>
                <Icon name={icon.draft} />
                <Step.Content>
                    <Step.Title>{lang.t('privateTitle')}</Step.Title>
                    <Step.Description>{lang.t('privateDesc')}</Step.Description>
                </Step.Content>
            </Step>
            <Step
                disabled={state <= 10 ? true : false}
                active={state > 10 ? state <= 20 ? true : false : false}>
                <Icon name={icon.open} />
                <Step.Content>
                    <Step.Title>{lang.t('openTitle')}</Step.Title>
                    <Step.Description>{lang.t('openDesc')}</Step.Description>
                </Step.Content>
            </Step>
            <Step
                disabled={state <= 20 ? true : false}
                active={state > 20 ? state <= 30 ? true : false : false}>
                <Icon name={icon.commit} />
                <Step.Content>
                    <Step.Title>{lang.t('commitTitle')}</Step.Title>
                    <Step.Description>{lang.t('commitDesc')}</Step.Description>
                </Step.Content>
            </Step>
            <Step
                disabled={state <= 30 ? true : false}
                active={state > 30 ? true : false}>
                <Icon name={icon.publish} />
                <Step.Content>
                    <Step.Title>{lang.t('publishTitle')}</Step.Title>
                    <Step.Description>{lang.t('publishDesc')}</Step.Description>
                </Step.Content>
            </Step>
        </Step.Group>
    )
}

const ItemView = observer(() => {
    const { base, formSettings, entryStore = base.entry } = useStores()
    setPageInfo(base)

    //TODO debug
    let all = new Array()
    all.push(<br />)
    const fieldIds = Object.keys(entryStore.person).reverse()
    for (let i=0; i<fieldIds.length; i++) {

        const fieldId = fieldIds[i]
        const field = entryStore.person[fieldId]
        const keys = Object.keys(field)
        all.push(<p>{fieldId + ': ' + keys + '\n'}</p>)

        let source = []
        for (let i=0; i<keys.length; i++) {
            field[keys[i]].bible !== undefined &&
                source.push(<p>bible: {field[keys[i]].bible}</p>)
            if (field[keys[i]].other !== undefined) {
                let t = ''
                const other = field[keys[i]].other
                for (let i=0; i<other.length; i++) {
                    t += other[i].title + ' - '
                }
                source.push(<p>other: {t}</p>)
            }
                
        }
        all.push(source)
        all.push(<br />)
    }
    
    //INFO gibt ersten key zurück --> Object.keys(obj)[0]
    return (
        <main>
            <Form>
                <Grid stackable>
                    <Grid.Column width={12}>
                        <Grid stackable>
                            {formSettings[base.path]().map(set =>
                                <BwkFormField
                                    key={set.id}
                                    settings={set} />
                            )}
                        </Grid>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <h2>{base.category}</h2>
                        {base.loading}
                        <p>{base.id}</p>
                        <p>{base.title}</p>
                        {/* TODO
                        <br></br>
                        <br></br>
                        <p><b style={{fontSize: 40}}>- D E B U G -</b></p>
                        {all}
                        */}
                    </Grid.Column>
                    <Grid.Column width={16}>
                        <Grid className='mainButtons'>
                            <Grid.Column width={16}>
                                <SaveButtons base={base} />
                            </Grid.Column>
                            <Grid.Column width={16}>
                                <OnTheWay state={base.state} />
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                </Grid>
            </Form>
        </main>
    )
});

export default ItemView