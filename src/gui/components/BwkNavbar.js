import React, { useState } from 'react'
//import BwkNavbarItem from './BwkNavbarItem'
import { Menu, Checkbox } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import Doc from '../func/Doc'
import { useStores } from '../../hooks/useStores'

const BwkNavbar = () => {
    const { base, strings, listsSettings } = useStores()
    const [state, setState] = useState({
        activeItem: Doc.getPath(),
        entries: 'sub-entries'
    });

    const clickItem = (e, { to, sub }) => {
        setState({
            activeItem: to
        })
        // TODO
        //Doc.title(e.target.title)
        return;

        let subMenu = document.getElementById(state.entries)

        subMenu.style.display = 'none'
        if (sub != null) {
            subMenu.style.display = 'block'
        }
    }
	return (
        <nav>
            <Menu large pointing secondary vertical>
                <Menu.Item
                    as={Link}
                    to='/'
                    active={state.activeItem ===  '/'}
                    onClick={clickItem} >
                        {strings.t('overview')}
                </Menu.Item>
                <Menu.Item
                    as={Link}
                    to='#'
                    active={state.activeItem ===  '#'}
                    sub={state.entries}
                    onClick={clickItem} >
                        {strings.t('entries')}
                </Menu.Item>
                <Menu.Menu id={state.entries}>
                    {listsSettings.categories.map(key => {
                        return <Menu.Item
                            key={key}
                            as={Link}
                            to={'/' + key}
                            active={state.activeItem ===  '/' + key}
                            sub={state.entries}
                            onClick={clickItem} >
                                {strings.t('titles')[key]}
                        </Menu.Item>
                    })}
                </Menu.Menu>
                <Menu.Item
                    as={Link}
                    to='/users'
                    active={state.activeItem ===  '/users'}
                    onClick={clickItem} >
                        {strings.t('users')}
                </Menu.Item>
                <Menu.Item
                    as={Link}
                    to='/profile'
                    active={state.activeItem ===  '/profile'}
                    onClick={clickItem} >
                        {strings.t('profile')}
                </Menu.Item>
                {/*<BwkNavbarItem to='/' name='Übersicht' />
                <BwkNavbarItem name='Einträge' sub={ entries } />
                    <BwkNavbarItem to='/epoch' name='Epoch' sub={entries} />
                    <BwkNavbarItem to='/event' name='Event' sub={entries} />
                <BwkNavbarItem to='/users' name='Benutzer' />
                <BwkNavbarItem to='/profile' name='Mein Profil' />*/}
            </Menu>
            <div className='buttonToggleMode'>
                {strings.t('cards')} <Checkbox onChange={base.toggleMode} slider /> {strings.t('table')}
            </div>
        </nav>
    )
}

export default BwkNavbar