import React from 'react'
import { Modal, Header, Button } from 'semantic-ui-react'

const BwkModal = (props) => {
    return (
        <Modal
            open={props.open}
            onClose={props.onClose}
            closeOnDimmerClick={false}
            dimmer='blurring'
            size={props.size} >
            {props.title && <Header icon={props.icon} content={props.title} />}
            <Modal.Content scrolling>
                <Modal.Description>
                    {props.children}
                </Modal.Description>
            </Modal.Content>
            {props.buttons &&
                <Modal.Actions>
                    {props.buttons.map(button =>
                        <Button
                            key={button.name}
                            onClick={button.onClick}
                            primary={button.primary}>
                            {button.name}
                        </Button>
                    )}
                </Modal.Actions>
            }
        </Modal>
    )
}

export default BwkModal