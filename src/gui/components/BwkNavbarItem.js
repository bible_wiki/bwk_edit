import React from 'react'
import { Link } from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

const BwkNavbarItem = (props) => {
    const [state, setState] = useState({
        activeItem: '/'
    });

    clickItem = (e, { name }) => {
        setState({
            activeItem: name
        })
        // hide all submenu
        document.getElementsByTagName('nav')[0].querySelector('ul').style.display = 'none'
        
        //let sub = document.getElementById(props.sub)
        //if (sub != null) {
        //    sub.style.display = 'block'
        //}
    }

    return (
        <Menu.Item
            name={ props.to }
            active={state.activeItem ===  props.to}
            onClick={clickItem}
        >
            <Link to={ props.to }>
                <span>{ props.name }</span>
            </Link>
        </Menu.Item>
    )
}

export default BwkNavbarItem