import React, { createRef, useState } from 'react'
import { Grid, Input, Form, Label, Placeholder, Popup, Accordion, Icon } from 'semantic-ui-react'
import BwkInput from './input/BwkInput'
import BwkInputButton from './input/BwkInputButton'
import { useStores } from '../../hooks/useStores'

const BwkFormFieldWrapper = ({ settings }) => {
    const { base, strings } = useStores()
    const [state, setState] = useState({
        error: false,
        loadingData: false,
        openPopup: false,
    })
    const contextRef = createRef()

    const openPopup = () => setState({ openPopup: true })
    const closePopup = () => setState({ openPopup: false })
    const setError = (bool) => {
        setState({
            error: bool
        })
    }
    const setLoadingData = (bool) => {
        setState({
            loadingData: bool
        })
    }

	return (
        <Form.Field>
            {settings.label &&
                <label>
                    {base.loading ?
                        <Placeholder>
                            <Placeholder.Line length='short' />
                        </Placeholder> :
                        settings.label
                    }
                </label>
            }
            <Input fluid action>
                <input style={{ display: 'none' }} />
                <span ref={contextRef}></span>
                {settings.component.map(set => {
                    if (set.type.substring(0,3) === 'inp') {
                        set.fieldId = settings.fieldId || settings.id
                        set.error = state.error
                        set.loadingData = state.loadingData
                        set.loading = settings.loading
                        set.label = settings.label
                        set.onFocus = openPopup
                        set.onBlur = closePopup
                        set.setLoadingData = setLoadingData
                        set.setError = setError
                        return <BwkInput
                            key={set.id}
                            settings={set} />
                    } else if (set.type.substring(0,3) === 'btn') {
                        set.fieldId = settings.fieldId || settings.id
                        return <BwkInputButton
                            key={set.id}
                            settings={set} />
                    }
                    return null
                })}
            </Input>
            {state.error &&
                <Label pointing prompt>
                    {settings.errorText || strings.t('inputError')}
                </Label>
            }
            {base.inpDescEnable && settings.info &&
                <InputDescription
                    settings={settings}
                    context={contextRef}
                    open={state.openPopup} />
            }
        </Form.Field>
    )
}

const InputDescription = (props) => {
    const { base, strings, icon } = useStores()
    return (
        <Popup
            context={props.context}
            content={
                <div>
                    <Icon name={icon.info} color='blue' />
                    {props.settings.info}
                    {' '}
                    <div style={{
                            cursor: 'pointer',
                            color: 'darkred',
                            textDecoration: 'underline',
                            fontStyle: 'italic',
                            display: 'inline-block',
                        }}
                        onClick={base.deactivateInpDesc}>
                        {strings.t('deactivate')}
                    </div>
                </div>
            }
            position={props.settings.upward ? 'bottom left' : 'top left'}
            open={props.open}
            style={{filter: 'none'}}
            pinned
            flowing />
    )
}

const BwkFormField = ({ settings }) => {
    const { base } = useStores()
    const [state, setState] = useState({
        activeIndex: 0
    });

    const openAdditional = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = state
        const newIndex = activeIndex === index ? -1 : index
    
        setState({ activeIndex: newIndex })
    }

	return (
        <Grid.Column id={settings.id} width={settings.width}>
            <BwkFormFieldWrapper
                settings={settings} />
            {settings.additional &&
                (base.loading ?
                    <Placeholder>
                        <Placeholder.Line length='short' />
                    </Placeholder> :
                    <Accordion>
                        <Accordion.Title
                            style={{color: '#1279c6'}}
                            active={state.activeIndex === 1}
                            index={1}
                            onClick={openAdditional} >
                            <Icon name='dropdown' />
                            {settings.additional.accLabel}
                        </Accordion.Title>
                        <Accordion.Content
                            active={state.activeIndex === 1} >
                            {settings.additional.fields.map(set => {
                                set.fieldId = set.id
                                return <BwkFormFieldWrapper
                                    key={set.id}
                                    settings={set} />
                            })}
                        </Accordion.Content>
                    </Accordion>
                )
            }
        </Grid.Column>
    )
}

export default BwkFormField
export {
    BwkFormFieldWrapper
}