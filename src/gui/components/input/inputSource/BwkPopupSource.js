import React, { Fragment, useState } from 'react'
import { Button, Icon, Popup, Grid, Header, List, Tab } from 'semantic-ui-react'
import { BwkDropdown } from '../BwkInput'
import { useStores } from '../../../../hooks/useStores'

const isBiblePos = (value, choosed) => {
    if (true) { // TODO ist eine Bibelstelle + (value muss in eine standardform gebracht werden)
        if (choosed.includes(value)) {
            return 'exist'
        } else {
            return 'true'
        }
    } else {
        return 'false'
    }
}

const SourceEmpty = () => {
    const { strings } = useStores()
    return (
        <div style={{textAlign: 'center', color: 'gray'}}>
            <i>{strings.t('noSource')}</i>
        </div>
    )
}

const ContentInput = ({ fieldId, items, value }) => {
    const { base, strings, icon, entryStore = base.entry, listsSettings } = useStores()
    const [state, setState] = useState({
        correct: 'false',
    })
    
    const search = (e) => setState({correct: isBiblePos(e.target.value, items)})

    const add = (event, { value: source }) => {
        if (state.correct === 'true') {
            entryStore.setBibleSource(fieldId, value, source)
            setState({correct: 'false'})
        }
    }

    return (
        <BwkDropdown
            className='bibleInput'
            settings={{
                id: listsSettings.source.bible,
                placeholder: strings.t('enterSource'),
                onSearchChange: search,
                focus: true,
                value: '',
                upward: true,
                style: {marginBottom: 15},
            }}
            onAddItem={add}
            additionLabel={state.correct === 'true' ?
                <Icon name={icon.add} color='green'/> :
                state.correct === 'exist' ?
                    <Icon name={icon.exist} color='yellow'/> :
                    <Icon name={icon.arrowRight} color='red'/>
            }
            allowAdditions
            noResult={null} />
    )
}

const ContentRows = ({ fieldId, value, items, settings }) => {
    const { base, icon, entryStore = base.entry } = useStores()
    let i=0

    return (
        items ?
        <List divided>
            {items.slice().reverse().map(item => {
                item.title && // is other
                    (settings.icon = icon.sourceOther[item.source] || icon.sourceOther.other)

                return <List.Item
                        key={i++} >
                        {item.title && // is other
                            <Icon
                                name={settings.icon}
                                color='grey'
                                onClick={() => settings.openModal.bind(this, item)}
                                style={{marginRight: 10, float: 'left', cursor: 'pointer'}} />
                        }
                        <span
                            onClick={() => item.title ? // is other
                                settings.openModal.bind(this, item) :
                                console.log('open --> ' + item)
                            }
                            style={{cursor: 'pointer'}} >
                                {item.title || item}
                        </span>
                        <Icon
                            onClick={() => item.title ? // is other
                                entryStore.removeOtherSource(fieldId, value, item.title) :
                                entryStore.removeBibleSource(fieldId, value, item)
                            }
                            name={icon.remove}
                            color='grey'
                            style={{float: 'right', cursor: 'pointer'}}/>
                    </List.Item>
            })}
        </List> :
        <SourceEmpty />
    )
}

const ContentColumns = ({value, name, settings}) => {
    const { base, strings, listsSettings, entryStore = base.entry } = useStores()
    const source = entryStore.getSource(settings.fieldId)
    const bibleOther = source[value] ? source[value][name] : []
    const title = name === listsSettings.source.bible ?
        strings.t('biblePlural') :
        name === listsSettings.source.other ? strings.t('otherPlural') : ''

    return (
        <Fragment>
            <Header as='h3' textAlign='center'>{title}</Header>
            {name === listsSettings.source.bible ?
                <ContentInput
                    fieldId={settings.fieldId}  
                    items={bibleOther}
                    value={value} /> :
                    <Button
                        style={{marginBottom: 15}}
                        onClick={settings.openModal.bind(this, null)}
                        fluid >
                        {strings.t('add')}
                    </Button>}
            <div style={{overflowY: 'auto', maxHeight: 100}}>
                {bibleOther !== undefined && bibleOther.length > 0 ? // nicht leer
                    <ContentRows
                        fieldId={settings.fieldId}
                        value={value}
                        items={bibleOther}
                        settings={settings} /> :
                    <SourceEmpty />
                }
            </div>
        </Fragment>
    )
}

const ContentTab = ({value, settings}) => {
    const { strings, listsSettings } = useStores()
    return (
        <Grid centered divided columns={settings.columnsPopup}>
            <Grid.Column>
                <ContentColumns
                    value={value}
                    name={listsSettings.source.bible}
                    settings={settings}
                />
                {settings.columnsPopup === 1 &&
                    <Button
                        style={{marginTop: 20}}
                        onClick={settings.openModal.bind(this, null)}
                        fluid >
                        {strings.t('otherSingular')}
                    </Button>
                }
            </Grid.Column>
            {settings.columnsPopup === 2 &&
                <Grid.Column>
                    <ContentColumns
                        value={value}
                        name={listsSettings.source.other}
                        settings={settings}
                    />
                </Grid.Column>
            }
        </Grid>
    )
}

const Content = ({settings}) => {
    const { base, entryStore = base.entry } = useStores()
    const value = entryStore.getValue(settings.fieldId, settings.id) || ['empty']

    let tabItems =[]
    if (value.length !== 1) {
        for (let i=0; i<value.length; i++) {
            tabItems.push({
                menuItem: value[i],
                render: () => <ContentTab
                            value={value[i]}
                            settings={settings} />
            })
        }
    }

    return (
        <Fragment>
            {value.length !== 1 ?
                <Tab
                    menu={{secondary: true, pointing: true}}
                    panes={tabItems} /> :
                <ContentTab
                    value={value[0]}
                    settings={settings} />
            }
        </Fragment>
    )
}

const PopupSource = ({settings}) => {
    return (
        <Popup
            context={settings.context}
            content={
                <Content
                    settings={settings} />
            }
            style={{width: settings.widthPopup, top: 20, left: 15}}
            position='bottom right'
            on='click'
            open={settings.popupIsOpen}
            onClose={settings.closePopup}
            pinned
            flowing />
    )
}

export default PopupSource