import React, { Fragment, createRef, useState } from 'react'
import { Default } from '../BwkInputButton'
import PopupSource from './BwkPopupSource'
import ModalSource from './BwkModalSource'
import { useStores } from '../../../../hooks/useStores'

const HasOther = (obj) => {
    const { listsSettings, key = listsSettings.source.other } = useStores()
    let bool = false
    if (obj !== undefined) {
        // falls eine Liste
        Object.keys(obj).forEach(function(keys) {
            keys === key && (bool = true)
        })
        // falls mehrere Listen
        if (!bool) {
            for (let i in obj) {
                Object.keys(obj[i]).forEach(function(keys) {
                    keys === key && (bool = true)
                })
            }
        }
    }
    return bool
}

const Source = ({ settings }) => {
    const { base, icon, entryStore = base.entry } = useStores()
    const [state, setState] = useState({
        popupIsOpen: false,
        modalIsOpen: false,
        currentOtherSource: null,
    })

    const contextRef = createRef()
    const openPopup = () => setState({ popupIsOpen: true })
    const closePopup = () => setState({ popupIsOpen: false })
    const openModal = (source) => {
        console.log(source)
        setState({ modalIsOpen: true, popupIsOpen: false, currentOtherSource: source })
    }
    const closeModal = () => setState({ modalIsOpen: false, popupIsOpen: true })

    const hasOther = HasOther(entryStore.getSource(settings.fieldId))
    settings.icon = icon.source
    settings.ref = <span ref={contextRef}></span>
    settings.onClick = openPopup
    settings.closePopup = closePopup
    settings.openModal = openModal
    settings.closeModal = closeModal
    settings.context = contextRef
    settings.popupIsOpen = state.popupIsOpen
    settings.modalIsOpen = state.modalIsOpen
    settings.widthPopup = hasOther ? 350 : 200
    settings.columnsPopup = hasOther ? 2 : 1
    settings.className = 'sourceButton'

    return (
        <Fragment>
            <Default
                settings={settings}>
                <span ref={contextRef}></span>
            </Default>
            <PopupSource
                settings={settings} />
            <ModalSource
                data={state.currentOtherSource}
                settings={settings} />
        </Fragment>
    )
}

export default Source