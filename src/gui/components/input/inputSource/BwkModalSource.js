import React from 'react'
import { Grid } from 'semantic-ui-react'
import { useStores } from '../../../../hooks/useStores'
import BwkModal from '../../template/BwkModal'
import BwkFormField from '../../BwkFormField'

const Content = ({ data, settings }) => {
    const { sourceSettings } = useStores()
    let i = 0

    //TODO data not undefined

    return (
        <Grid>
            {sourceSettings.get().map(set => {
                i++
                i === 1 && (set.focus = true)
                data && (set.value = data[set.id])
                return <BwkFormField
                    key={i}
                    settings={set} />
                }
            )}
        </Grid>
    )
}

const ModalSource = ({ data, settings }) => {
    const { strings, icon } = useStores()
    return (
        <BwkModal
            open={settings.modalIsOpen}
            onClose={settings.closeModal}
            title={strings.t('addOtherSource')}
            icon={icon.source}
            buttons={[
                {
                    name: strings.t('cancel'),
                    onClick: settings.closeModal,
                },
                {
                    name: strings.t('save'),
                    onClick: settings.closeModal,
                    primary: true,
                },
            ]} >
            <Content
                data={data}
                settings={settings} />
        </BwkModal>
    )
}

export default ModalSource