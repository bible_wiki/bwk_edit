import React, { Fragment, useState } from 'react'
import { Grid, Icon } from 'semantic-ui-react'
import { BwkDropdown } from './BwkInput'
import BwkModal from '../template/BwkModal'
import { useStores } from '../../../hooks/useStores'
import BwkFormField from '../BwkFormField'

const FromModal = ({ name, settings }) => {
    const { strings, formSettings } = useStores()
    let i = 0
    return (
        <BwkModal
            open={settings.open}
            onClose={settings.onClose}
            title={strings.t('newEntries')[settings.list]}
            buttons={[
                {
                    name: strings.t('cancel'),
                    onClick: settings.onClose,
                },
                {
                    name: strings.t('save'),
                    onClick: settings.onCloseSave,
                    primary: true,
                },
            ]} >
            <Grid>
                {formSettings[settings.list + 'New']().map(set => {
                    i++
                    if (i === 1) {
                        set.focus = true
                        set.value = name
                    }
                    return <BwkFormField
                        key={set.id}
                        settings={set} />
                })}
            </Grid>
        </BwkModal>
    )
}

const FromModalDropdown = ({fieldId, settings}) => {
    const { strings, icon, listsSettings } = useStores() 
    const [state, setState] = useState({
        openModal: false,
        value: '',
    });
    const openModal = (e) => setState({ openModal: true, value: e.target.value })
    const closeModal = () => setState({ openModal: false })
    const closeSaveModal = () => {
        console.log('save new entry')
        setState({ openModal: false })
    }
    settings.open = state.openModal
    settings.onClose = closeModal
    settings.onCloseSave = closeSaveModal

    let withModal
    listsSettings.routesLoadList.includes(settings.list) ?
        (withModal = true) :
        (withModal = false)

    return (
        <Fragment>
            <BwkDropdown
                fieldId={fieldId}
                settings={settings}
                noResult={settings.noResult || null}
                onAddItem={withModal ? openModal : null}
                additionLabel={
                    <Fragment>
                        <span style={{color: 'green'}}>
                            {strings.t('create')}
                        </span>
                        <Icon name={icon.arrowRight} color='green' />
                    </Fragment>
                }
                additionPosition='bottom'
                allowAdditions
            />
            {withModal &&
                <FromModal
                    name={state.value}
                    settings={settings} />
            }
        </Fragment>
    )
}

export default FromModalDropdown