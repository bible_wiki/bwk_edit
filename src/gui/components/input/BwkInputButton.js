import React from 'react'
import { Button, Icon } from 'semantic-ui-react'
import { useStores } from '../../../hooks/useStores'
import Source from './inputSource/BwkButtonSource'
import DateRoughly from './inputDate/BwkButtonDateRoughly'

const Default = (props) => {
    const { base } = useStores()
    const { settings } = props
    return (
        <Button
            onClick={settings.onClick}
            disabled={base.loading}
            active={props.active}
            primary={props.primary}
            style={props.style}
            basic={settings.basic}
            className={settings.className}
            compact >
            {settings.icon &&
                <Icon name={settings.icon} />
            }
            {settings.name}
            {props.children}
        </Button>
    )
}

const Group = ({ settings }) => {
    const { base, entryStore = base.entry } = useStores()
    
    const onButtonClick = (e) => {
        let buttons = e.target.parentElement
        for(let i=0; i<buttons.childElementCount; i++){
            buttons.children[i].classList.remove('active')
            buttons.children[i].classList.remove('primary')
        }
        e.target.classList.add('active')
        e.target.classList.add('primary')

        for(let i=0; i<buttons.childElementCount; i++){
            if (buttons.children[i].classList.contains('active')) {
                entryStore.setValue(settings.fieldId, i)
                break
            }
        }
    }
    const value = Number(entryStore.getValue(settings.fieldId, settings.id))
	return (
        <Button.Group style={settings.id !== 'isbn' ? { width: 'calc(100% - 42px)' } : {}} >
            {settings.component.map((set, i) => {
                set.onClick = onButtonClick
                set.basic = true
                return <Default
                    key={set.id}
                    settings={set}
                    active={value === i}
                    primary={value === i}
                    style={{width: 'calc(100% / 3)'}} />
            })}
        </Button.Group>
    )
}

const BwkInputButton = ({ settings }) => {

    const types = {
        Default: Default,
        Group: Group,
        Source: Source,
        DateRoughly: DateRoughly,
    }
    const CustomButton = types[settings.type.substr(3)]
        
    return <CustomButton settings={settings} />
}

export default BwkInputButton
export {Default}