import React from 'react'
import { Popup, Grid, Header } from 'semantic-ui-react'
import { useStores } from '../../../../hooks/useStores'
import { BwkFormFieldWrapper } from '../../BwkFormField'
import { observer } from 'mobx-react'

const Content = observer(({ data, settings }) => {
    const { strings, listsSettings } = useStores()

    return (
        <Grid centered divided columns={settings.columnsPopup}>
            <Grid.Column>
                <Header as='h3' textAlign='center'>{strings.t('roughly')}</Header>
                <BwkFormFieldWrapper
                    settings={{
                        id: settings.fieldId,
                        component: [
                            {
                                id: settings.fieldId + 'RoughlyNumb',
                                type: listsSettings.types.inpNoDropdown,
                                value: data.numb === 0 ? '' : data.numb,
                                pattern: '[0-9]{1,4}',
                                focus: true,
                                upward: true,
                                style: {width: 70},
                            },
                            {
                                id: settings.fieldId + 'RoughlyPeriod',
                                type: listsSettings.types.inpFromDropdown,
                                value: data.numb === 1 ? strings.t(data.period) : strings.t(data.period + 's'),
                                list: listsSettings.lists.roughlyPeriod,
                                onlyChoose: true,
                                style: {width: 90},
                            },
                        ]
                    }} />
            </Grid.Column>
        </Grid>
    )
})

const PopupDateRoughly = ({ data, settings }) => {
    return (
        <Popup
            context={settings.context}
            content={
                <Content
                    data={data}
                    settings={settings} />
            }
            style={{top: 20, left: 15}}
            position='bottom right'
            on='click'
            open={settings.popupIsOpen}
            onClose={settings.closePopup}
            pinned
            flowing />
    )
}

export default PopupDateRoughly