import React, { Fragment, createRef, useState } from 'react'
import { Default } from '../BwkInputButton'
import { useStores } from '../../../../hooks/useStores'
import PopupDateRoughly from './BwkPopupDateRoughly'

const DateRoughly = ({ settings }) => {
    const { base, entryStore = base.entry } = useStores()
    const [state, setState] = useState({
        popupIsOpen: false,
    })

    const contextRef = createRef()
    const openPopup = () => setState({ popupIsOpen: true })
    const closePopup = () => setState({ popupIsOpen: false })

    settings.ref = <span ref={contextRef}></span>
    settings.onClick = openPopup
    settings.closePopup = closePopup
    settings.context = contextRef
    settings.popupIsOpen = state.popupIsOpen
    settings.name = '± ' + entryStore.getValue(settings.fieldId, 'Roughly')
    settings.basic = true
    settings.className = 'sourceButton'

    return (
        <Fragment>
            <Default
                settings={settings}>
                <span ref={contextRef}></span>
            </Default>
            <PopupDateRoughly
                data={entryStore.getValue(settings.fieldId, 'Roughly', true)}
                settings={settings} />
        </Fragment>
    )
}

export default DateRoughly