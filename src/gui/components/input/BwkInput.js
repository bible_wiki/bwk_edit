import React from 'react'
import { Dropdown, Input } from 'semantic-ui-react'
import FromModalDropdown from './BwkInputFromModalDropdown'
import { useStores } from '../../../hooks/useStores'

const getValueByKey = (key, options) => {
    for (let i=0; i<options.length; i++) {
        if (options[i].key === key) {
            return options[i].value
        }
    }
}

const getValuesByKeys = (keys, options) => {
    let values = new Array()
    for (let i=0; i<keys.length; i++) {
        for (let j=0; j<options.length; j++) {
            if (options[j].key === keys[i]) {
                values.push(options[j].value)
            }
        }
    }
    return values
}

const getKeyByValue = (value, options) => {
    for (let i=0; i<options.length; i++) {
        if (options[i].value === value) {
            return options[i].key
        }
    }
}

const getKeysByValues = (values, options) => {
    let keys = new Array()
    for (let i=0; i<values.length; i++) {
        for (let j=0; j<options.length; j++) {
            if (options[j].value === values[i]) {
                keys.push(options[j].key)
            }
        }
    }
    return keys
}

const BwkDropdown = (props) => {
    const { base, listsSettings, entryStore = base.entry, lists } = useStores()
    const { settings } = props
    const value = settings.value || entryStore.getValue(settings.fieldId, settings.id)
    const options = lists[settings.list] ||
        (typeof listsSettings[settings.list] === 'function' &&
            listsSettings[settings.list](value)
        ) || []
    
    return (
        <Dropdown
            defaultValue={settings.multiple ?
                //kann mehrere Eingaben haben
                (Array.isArray(value) ? settings.onlyKey ? getValuesByKeys(value, options) : value : new Array(settings.onlyKey ? getValueByKey(value, options) : value)) :
                //kann nur eine Eingabe haben
                (Array.isArray(value) ? settings.onlyKey ? getValueByKey(value[0], options) : value[0] : settings.onlyKey ? getValueByKey(value, options) : value)
            }
            placeholder={settings.placeholder ?? settings.label}
            options={options}
            className={props.className}
            name={settings.id}
            error={settings.error}
            disabled={base.loading || settings.disabled}
            loading={settings.loadingData}
            multiple={settings.multiple}
            noResultsMessage={props.noResult}
            onFocus={settings.onFocus}
            onBlur={settings.onBlur}
            onSearchChange={settings.onSearchChange}
            onChange={settings.onChange}
            onAddItem={props.onAddItem}
            additionLabel={props.additionLabel}
            additionPosition={props.additionPosition}
            allowAdditions={props.allowAdditions}
            searchInput={{
                autoFocus: settings.focus,
                type: settings.htmlType,
            }}
            icon={settings.multiple || value === undefined || value[0] === 'empty' || settings.onlyChoose ? null : 'dropdown'}
            selectOnNavigation={false}
            upward={settings.upward}
            style={settings.style}
            clearable
            fluid
            search={!settings.onlyChoose}
            selection
            scrolling
            compact
        />
    )
}

const NoDropdown = ({settings}) => {
    const { base, entryStore = base.entry } = useStores()
    const value = settings.value || entryStore.getValue(settings.fieldId, settings.id)

    return (
        <Input
            //TODO false
            value={Array.isArray(value) ? value[0] : value}
            placeholder={settings.placeholder || settings.label}
            name={settings.id}
            type={settings.htmlType}
            label={settings.prefix}
            error={settings.error}
            disabled={base.loading || settings.disabled}
            loading={settings.loadingData}
            onFocus={settings.onFocus}
            onBlur={settings.onBlur}
            onChange={settings.onChange}
            focus={settings.focus}
            style={settings.style || {width: '100%'}}
            fluid
        />
    )
}
const WithDropdown = ({settings}) => {
    return (
        <BwkDropdown
            settings={settings}
            noResult={settings.noResult || null}
        />
    )
}
const FromDropdown = ({settings}) => {
    const { strings } = useStores()
    return (
        <BwkDropdown
            settings={settings}
            noResult={settings.noResult || strings.t('chooseOne')}
        />
    )
}

const BwkInput = (props) => {
    const { base, entryStore = base.entry } = useStores()
    const { settings } = props
    const types = {
        NoDropdown: NoDropdown,
        WithDropdown: WithDropdown,
        FromDropdown: FromDropdown,
        FromModalDropdown: FromModalDropdown
    }
    const CustomInput = types[settings.type.substr(3)]

    const isCorrect = (value) => value !== '' || value.match(settings.pattern) !== null
    const saveInput = (event, { value, options }) => {
        settings.setLoadingData(true)

        if (settings.onlyKey)
            entryStore.setValue(settings.fieldId, Array.isArray(value) ? getKeysByValues(value, options) : getKeyByValue(value, options), settings.id)
        else if (isCorrect(value)) {
            settings.setError(false)
            entryStore.setValue(settings.fieldId, value, settings.id)
        } else settings.setError(true)

        settings.setLoadingData(false)
    }

    settings.onChange = saveInput
    
    return <CustomInput settings={settings} />
}

export default BwkInput
export {BwkDropdown}