import React from 'react'
import { useStores } from '../../hooks/useStores'
import ItemView from '../pages/ItemView'
import ListView from '../pages/ListView'
import NotFound from '../pages/NotFound'
import Doc from './Doc'

const Path = () => {
	const { base, listsSettings } = useStores()

	if (listsSettings.routesLoadItem.includes(base.path)) {
		return <ItemView />
	} else if (listsSettings.routesLoadList.includes(base.path)) {
		return <ListView />
	} else if (listsSettings.profile == base.path) {
		Doc.link('https://portal.biblewiki.one/profile')
	} else {
		return <NotFound />
	}
}

export default Path