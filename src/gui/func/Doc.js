import { createBrowserHistory } from 'history'
import { useStores } from '../../hooks/useStores'

class Doc {
    isDefault = 'default'
    isCurrent = 'current'

    static setDefault = () => {
        this.info(this.isDefault, this.isDefault, this.isDefault)
    }

    static info = (title, url, src) => {
        this.title(title)
        this.url(url)
        this.icon(src)
    }

    static title = (title) => {
        if (title === this.isDefault) {
            document.title = useStores().strings.t('appName')
        } else if (title === this.isCurrent) {
            // do nothing
        } else {
            document.title = title + ' ⬛️ ' + useStores().strings.t('appName')
        }
    }

    static url = (url) => {
        if (url === this.isDefault) {
            createBrowserHistory().replace('/')
        } else if (url === this.isCurrent) {
            // do nothing
        } else {
            createBrowserHistory().replace('/' + url)
        }
    }

    static link = (url) => {
        window.open(url, '_self')
    }

    static icon = (src) => {
        // TODO
        return
        let link = document.querySelector('link[rel*="icon"]') || document.createElement('link')
        link.type = 'image/svg+xml'
        link.rel = 'icon'

        if (src === this.isDefault) {
            link.href = '../../logo.svg'
            document.getElementsByTagName('head')[0].appendChild(link)
        } else if (src === this.isCurrent) {
            // do nothing
        } else {
            link.href = src
            document.getElementsByTagName('head')[0].appendChild(link)
        }
    }

    static lang = (lang) => {
        document.documentElement.lang = lang
    }

    static getPath = () => ('/' + window.location.pathname.split('/')[1])
    static getId = () => ('/' + window.location.pathname.split('/')[2])
}

export default Doc