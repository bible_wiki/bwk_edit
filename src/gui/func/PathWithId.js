import React from 'react'
import { useStores } from '../../hooks/useStores'
import ItemView from '../pages/ItemView'
import ListView from '../pages/ListView'
import NotFound from '../pages/NotFound'

const PathWithId = () => {
	const { base, listsSettings } = useStores()

	if (listsSettings.routesWithIdLoadItem.includes(base.path)) {
		return <ItemView />
	} else if (listsSettings.routesWithIdLoadList.includes(base.path)) {
		return <ListView />
	} else {
		return <NotFound />
	}
}

export default PathWithId